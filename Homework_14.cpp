﻿
#include <iostream>
#include <string>

int main()
{
    
    std::string str("Homework");

    //Выводим переменную str
    std::cout << str << "\n";

    //Выводим длину строки str
    std::cout << str.length() << "\n";

    //Выводим первый символ строки str
    std::cout << str[0] << "\n";

    //Выводим последний символ строки str
    std::cout << str[str.length() - 1] << "\n";

}

